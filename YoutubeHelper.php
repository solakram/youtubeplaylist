<?php

class YoutubeHelper {

	private $stats = [];

	public function __construct() {
		$client = new Google_Client();
		$client->setAuthConfig(dirname(__FILE__).'/auth/client_secrets.json');
		$client->addScope(Google_Service_YouTube::YOUTUBE_FORCE_SSL);
		$client->setAccessToken($_SESSION['access_token']);
		$this->client = $client;
		$this->youtubeClient = new Google_Service_YouTube($client);
	}

	public function getStats() {
		return $this->stats;
	}

	private function createPlaylistItemFromVideoId($videoId, $playlistId) {
		$item = new Google_Service_YouTube_PlaylistItem();
		$snippet = new Google_Service_YouTube_PlaylistItemSnippet();
		$resourceId = new Google_Service_YouTube_ResourceId();
		$resourceId->setVideoId($videoId);
		$resourceId->setKind('youtube#video');
		$snippet->setResourceId($resourceId);
		$snippet->setPlaylistId($playlistId);
		$item->setSnippet($snippet);
		return $item;
	}

	public function deletePlaylistItemsFromPlaylist($playlistId) {
		$deletedVideos = 0;
		try {
			while($playlistItemIds = $this->filterPlaylistItemIdsFromPlaylistItems($this->getPlaylistItems($playlistId))) {
				foreach($playlistItemIds as $playlistItemId) {
					$this->youtubeClient->playlistItems->delete($playlistItemId);
					$deletedVideos++;
				}
			}
		}catch (Exception $e) {
			$this->stats['Exception Delete Videos'] = html_entity_decode($e->getMessage());
		}
		$this->stats['Deleted Videos'] = $deletedVideos;
	}

	public function getInsertablePlaylists($playlistIds) {
		$playlists = array();
		foreach($playlistIds as $playlistId) {
			$playlists[] = $this->filterVideoIdsFromPlaylistItems($this->getPlaylistItems($playlistId));
		}
		return $playlists;
	}

	public function addPlaylistItemsToPlaylistByVideoIds($videoIds, $playlistId) {
		$addedVideos = 0;
		foreach ($videoIds as $videoId) {
			try {
				$this->youtubeClient->playlistItems->insert('snippet', $this->createPlaylistItemFromVideoId($videoId, $playlistId));
				$addedVideos++;
			} catch (Exception $e) {
				$this->stats['Exception Adding Video'] = $e->getMessage();
			}
		}
		$this->stats['Added Videos'] = $addedVideos;
	}

	private function filterPlaylistItemIdsFromPlaylistItems($playlistItems) {
		$ids = array();
		/**
		 * @var $playlistItem Google_Service_YouTube_PlaylistItem
		 */
		foreach($playlistItems->getItems() as $playlistItem) {
			$ids[] = $playlistItem->getId();
		}

		return $ids;
	}

	private function filterVideoIdsFromPlaylistItems($playlistItems) {
		$ids = array();
		/**
		 * @var $playlistItem Google_Service_YouTube_PlaylistItem
		 */
		foreach($playlistItems->getItems() as $playlistItem) {
			$ids[] = $playlistItem->getSnippet()['modelData']['resourceId']['videoId'];
		}

		return $ids;
	}

	/**
	 * @param $playlistId
	 * @return Google_Service_YouTube_PlaylistItemListResponse
	 */
	private function getPlaylistItems($playlistId) {
		return $this->youtubeClient->playlistItems->listPlaylistItems('snippet', array(
			'playlistId' => $playlistId,
			'maxResults' => 50
		));
	}
}