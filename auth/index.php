<?php
require_once __DIR__.'/../vendor/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfig(dirname(__FILE__).'/client_secrets.json');
$client->addScope(Google_Service_YouTube::YOUTUBE_FORCE_SSL);
$config = json_decode(file_get_contents("../config.json"));
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
	$redirect_uri = $config->baseUrl;
	header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
} else {
	$redirect_uri = $config->baseUrl.'auth/oauth2callback.php';
	header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}